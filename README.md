# 4chan media scraper
Currently hardcoded to loop through the JSON used to create the catalog of /b/: http://boards.4chan.org/b/catalog  
4chans JSON found here: https://github.com/4chan/4chan-API  
All threads: https://a.4cdn.org/b/catalog.json  
Specific thread: https://a.4cdn.org/b/thread/817798881.json  
Media: either http://i.4cdn.org/b/1576942272966.webm or http://is2.4chan.org/b/1576942272966.webm
