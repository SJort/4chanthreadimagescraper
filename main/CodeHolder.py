# import urllib.request
# import json
# import os
#
#
# def go(board='wsg', thread='ylyl', folder='images'):
#     url = 'https://a.4cdn.org/' + board + '/'
#     print('Scraping', url)
#     catalog_url = urllib.request.urlopen('https://a.4cdn.org/wsg/catalog.json')
#
#
# with urllib.request.urlopen('https://a.4cdn.org/wsg/catalog.json') as url:
#     data = json.loads(url.read())
#     print('Data:', type(data))
#     print('Length:', len(data))
#
#     threads_ids = []
#     for datum in data:
#         for sub in datum['threads']:
#             try:
#                 value = sub['sub']
#                 value = value.lower()
#                 if 'ylyl' in value:
#                     thread = sub['no']
#                     threads_ids.append(thread)
#
#             except:
#                 pass
#
#     threads = []
#     print('Found threads ids:')
#     for thread_id in threads_ids:
#         print(thread_id)
#         # example: from 3205544 to https://a.4cdn.org/wsg/thread/3205544.json
#         thread_url = 'https://a.4cdn.org/wsg/thread/' + str(thread_id) + '.json'
#         print('Reading from', thread_url)
#         with urllib.request.urlopen(thread_url) as thread_url:
#             threads.append(json.loads(thread_url.read()))
#
#     image_urls = []
#     for thread in threads:
#         for post in thread['posts']:
#             try:
#                 image_id = post['tim']
#                 image_type = post['ext']
#                 # http://i.4cdn.org/wsg/1576942272966.webm
#                 image_url = 'http://i.4cdn.org/wsg/' + str(image_id) + str(image_type)
#                 image_urls.append(image_url)
#             except:
#                 pass
#
#     for image_url in image_urls:
#         filename = image_url.split('/')[-1]
#         path = 'images/' + filename
#         already_exists = False
#         for x in os.listdir('images'):
#             if filename in x:
#                 already_exists = True
#                 break
#
#         if already_exists:
#             break
#
#         print('Downloading', filename, '...')
#         image = urllib.request.urlretrieve(image_url, path)
#         print('Image type:', type(image))
#         print('Image:', image)
