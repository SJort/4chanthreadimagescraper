import json
import os
import sys
import urllib.request
import urllib.error
import traceback

print_verbose = 0

image_base_urls = ("https://a.4cdn.org/", "http://is2.4chan.org/")


def log(*args):
    if print_verbose == 1:
        print(*args)


def download_images(board, thread_path, posts):
    # download the image from each post if it exists
    for post in posts:
        # determine filename
        try:
            image_name = post["tim"]  # name
            image_extension = post["ext"]  # .webm
        except KeyError:
            # not every post has an image
            continue

        # name.webm
        image_filename = str(image_name) + str(image_extension)
        # skip if already downloaded
        image_save_path = os.path.join(thread_path, image_filename)
        if os.path.exists(image_save_path):
            print(f"{image_save_path} already exists.")
            return

        # try to download from all possible urls
        global image_base_urls
        for image_base_url in image_base_urls:
            try:
                # http://i.4cdn.org/wsg/1576942272966.webm
                image_url = f"{image_base_url}/{board}/{image_filename}"
                print(f"Trying to download {image_url}")
                urllib.request.urlretrieve(image_url, image_save_path)
                # stop trying if downloaded
                print("Downloaded!")
                break
            except Exception as e:
                print(f"Failed: {e}")


def go(board="b", thread_name_to_match="ylyl", folder="/home/jort/this/parser/threads", thread_name_id="com",
       verbose=0):
    # hackerman
    verbose = int(verbose)
    global print_verbose
    print_verbose = verbose

    log("Executing with board =", board, "match =", thread_name_to_match, "folder =", folder, "name_id =",
        thread_name_id)
    image_url_1 = f"https://a.4cdn.org/{board}/"
    image_url_2 = f"http://is2.4chan.org/{board}/"
    log("Scraping", image_url_1)
    catalog_url = image_url_1 + "catalog.json"
    log("Catalog:", catalog_url)
    catalog = urllib.request.urlopen(catalog_url)

    catalog_data = json.loads(catalog.read())

    threads_ids = []
    for catalog_datum in catalog_data:
        for thread_item in catalog_datum["threads"]:
            try:
                thread_name = thread_item[thread_name_id]
                thread_name = thread_name.lower()
                if thread_name_to_match in thread_name:
                    thread_id = thread_item["no"]
                    threads_ids.append(thread_id)
            except KeyError:
                log("Key not found:", thread_name_id)
                pass
            except Exception:
                traceback.print_exc()

    log("Done parsing thread ids")

    for thread_id in threads_ids:
        # example: from 3205544 to https://a.4cdn.org/wsg/thread/3205544.json
        thread_url = image_url_1 + "thread/" + str(thread_id) + ".json"
        log("Reading thread", thread_url)
        thread_data = urllib.request.urlopen(thread_url)
        thread_data = json.loads(thread_data.read())

        thread_folder_path = folder + "/" + str(thread_id)
        log("Thread folder:", thread_folder_path)
        if not os.path.exists(thread_folder_path):
            os.makedirs(thread_folder_path)

        download_images(board, thread_folder_path, thread_data["posts"])



try:
    log("Args:", sys.argv)
    if len(sys.argv) != 6:
        exit("Invalid amount of arguments")

    arg1 = sys.argv[1]  # board
    arg2 = sys.argv[2]  # search term
    arg3 = sys.argv[3]  # folder
    arg4 = sys.argv[4]  # thread name id, see json
    arg5 = sys.argv[5]  # verbosity level
    # b: thread_name = "com"
    # wsg: thread_name = "sub"

    try:
        go(board=arg1, thread_name_to_match=arg2, folder=arg3, thread_name_id=arg4, verbose=arg5)
    except Exception:
        traceback.print_exc()
        exit("Exception in main function")
except KeyboardInterrupt:
    exit("Program interrupted")
