import json
import os
import sys
import urllib.request
import urllib.error
import traceback

print_verbose = 0


def log(*args):
    if print_verbose == 1:
        print(*args)


def go(board='b', thread_name_to_match='ylyl', folder='/home/jort/this/parser/threads', thread_name_id='com',
       verbose=0):
    # hackerman
    verbose = int(verbose)
    global print_verbose
    print_verbose = verbose

    log('Executing with board =', board, 'match =', thread_name_to_match, 'folder =', folder, 'name_id =',
          thread_name_id)
    url = 'https://a.4cdn.org/' + board + '/'
    url2 = 'http://is2.4chan.org/b/'
    log('Scraping', url)
    catalog_url = url + 'catalog.json'
    log('Catalog:', catalog_url)
    catalog = urllib.request.urlopen(catalog_url)

    catalog_data = json.loads(catalog.read())

    threads_ids = []
    for catalog_datum in catalog_data:
        for thread_item in catalog_datum['threads']:
            try:
                thread_name = thread_item[thread_name_id]
                thread_name = thread_name.lower()
                if thread_name_to_match in thread_name:
                    thread_id = thread_item['no']
                    threads_ids.append(thread_id)
            except KeyError:
                log('Key not found:', thread_name_id)
                pass
            except Exception:
                traceback.print_exc()

    log('Done parsing thread ids')

    for thread_id in threads_ids:
        # example: from 3205544 to https://a.4cdn.org/wsg/thread/3205544.json
        thread_url = url + 'thread/' + str(thread_id) + '.json'
        log('Reading thread', thread_url)
        thread_data = urllib.request.urlopen(thread_url)
        thread_data = json.loads(thread_data.read())

        thread_folder = folder + '/' + str(thread_id)
        log('Thread folder:', thread_folder)
        if not os.path.exists(thread_folder):
            os.makedirs(thread_folder)

        for post in thread_data['posts']:
            # not every post has an image
            try:
                image_id = post['tim']
                image_type = post['ext']
            except KeyError:
                continue

            # http://i.4cdn.org/wsg/1576942272966.webm
            image_filename = str(image_id) + str(image_type)

            image_url = url + image_filename
            # log('Image URL:', image_url)

            image_path = thread_folder + '/' + image_filename
            # log('Image path:', image_path)

            log('Downloading', image_filename)
            if not os.path.exists(image_path):
                try:
                    urllib.request.urlretrieve(image_url, image_path)
                    log('Downloaded', image_url)
                except urllib.error.HTTPError:
                    image_url = url2 + image_filename
                    try:
                        log('Trying again with other image server')
                        log('Downloaded ', image_url)
                        urllib.request.urlretrieve(image_url, image_path)
                    except urllib.error.HTTPError:
                        log('HTTP error occurred while downloading')
                        traceback.print_exc()

            else:
                log('Image already downloaded')


try:
    log('Args:', sys.argv)
    if len(sys.argv) != 6:
        exit('Invalid amount of arguments')

    arg1 = sys.argv[1]  # board
    arg2 = sys.argv[2]  # search term
    arg3 = sys.argv[3]  # folder
    arg4 = sys.argv[4]  # thread name id, see json
    arg5 = sys.argv[5]  # verbosity level
    # b: thread_name = 'com'
    # wsg: thread_name = 'sub'

    try:
        go(board=arg1, thread_name_to_match=arg2, folder=arg3, thread_name_id=arg4, verbose=arg5)
    except Exception:
        traceback.print_exc()
        exit('Exception in main function')
except KeyboardInterrupt:
    exit('Program interrupted')
